# Kubernetes Logging
![alt text](overview.png "Overview")

## Create Namespace
```
$ kubectl create -f namespace-logging.json
```

## Install Helm
```
$ brew install kubernetes-helm
```

## Install Tiller with RBAC
```
$ kubectl create -f rbac-config.yaml
$ helm init --service-account tiller
```

## Deploy Kafka
```
$ helm repo add bitnami https://charts.bitnami.com/bitnami
$ helm install bitnami/kafka --namespace logging
NAME:   virtuous-pig
LAST DEPLOYED: Mon Jan 28 11:04:50 2019
NAMESPACE: logging
STATUS: DEPLOYED

RESOURCES:
==> v1/Service
NAME                             TYPE       CLUSTER-IP    EXTERNAL-IP  PORT(S)                     AGE
virtuous-pig-zookeeper-headless  ClusterIP  None          <none>       2181/TCP,2888/TCP,3888/TCP  1s
virtuous-pig-zookeeper           ClusterIP  10.0.248.151  <none>       2181/TCP,2888/TCP,3888/TCP  1s
virtuous-pig-kafka-headless      ClusterIP  None          <none>       9092/TCP                    1s
virtuous-pig-kafka               ClusterIP  10.0.39.29    <none>       9092/TCP                    1s

==> v1beta2/StatefulSet
NAME                    DESIRED  CURRENT  AGE
virtuous-pig-zookeeper  1        1        1s
virtuous-pig-kafka      1        1        1s

==> v1/Pod(related)
NAME                      READY  STATUS   RESTARTS  AGE
virtuous-pig-zookeeper-0  0/1    Pending  0         1s
virtuous-pig-kafka-0      0/1    Pending  0         1s


NOTES:


** Please be patient while the chart is being deployed **

Kafka can be accessed via port 9092 on the following DNS name from within your cluster:

    virtuous-pig-kafka.logging.svc.cluster.local

To create a topic run the following command:

    export POD_NAME=$(kubectl get pods --namespace logging -l "app=kafka" -o jsonpath="{.items[0].metadata.name}")
    kubectl exec -it $POD_NAME -- kafka-topics.sh --create --zookeeper virtuous-pig-zookeeper:2181 --replication-factor 1 --partitions 1 --topic test

To list all the topics run the following command:

    export POD_NAME=$(kubectl get pods --namespace logging -l "app=kafka" -o jsonpath="{.items[0].metadata.name}")
    kubectl exec -it $POD_NAME -- kafka-topics.sh --list --zookeeper virtuous-pig-zookeeper:2181

To start a kafka producer run the following command:

    export POD_NAME=$(kubectl get pods --namespace logging -l "app=kafka" -o jsonpath="{.items[0].metadata.name}")
    kubectl exec -it $POD_NAME -- kafka-console-producer.sh --broker-list localhost:9092 --topic test

To start a kafka consumer run the following command:

    export POD_NAME=$(kubectl get pods --namespace logging -l "app=kafka" -o jsonpath="{.items[0].metadata.name}")
    kubectl exec -it $POD_NAME -- kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning

To connect to your Kafka server from outside the cluster execute the following commands:

    kubectl port-forward --namespace logging svc/virtuous-pig-kafka 9092:9092 &
    echo "Kafka Broker Endpoint: 127.0.0.1:9092"

    PRODUCER:
        kafka-console-producer.sh --broker-list 127.0.0.1:9092 --topic test
    CONSUMER:
        kafka-console-consumer.sh --bootstrap-server 127.0.0.1:9092 --topic test --from-beginning
```

## Deploy Fluentbit
```
$ kubectl create -f fluent-bit-service-account.yaml --namespace logging
$ kubectl create -f https://raw.githubusercontent.com/fluent/fluent-bit-kubernetes-logging/master/fluent-bit-role.yaml
$ kubectl create -f https://raw.githubusercontent.com/fluent/fluent-bit-kubernetes-logging/master/fluent-bit-role-binding.yaml
```

Updated `Brokers` and `Topics` to `virtuous-pig-kafka.logging.svc.cluster.local:9092` and `logging`
![alt text](fluentbit-configmap.png "Fluentbit ConfigMap")
```
$ kubectl create -f fluent-bit-configmap.yaml --namespace logging
$ kubectl create -f fluent-bit-ds.yaml --namespace logging
```
## Deploy Nifi
```
$ kubectl create -f https://raw.githubusercontent.com/whs-dot-hk/kubernetes-nifi-refined/master/nifi.yaml --namespace logging
$ kubectl create -f https://raw.githubusercontent.com/whs-dot-hk/kubernetes-nifi-refined/master/nifi-service.yaml --namespace logging
$ kubectl create -f https://raw.githubusercontent.com/whs-dot-hk/kubernetes-nifi-refined/master/frontend.yaml --namespace logging
```
![alt text](pods.png "Pods")
```
$ export POD_NAME=$(kubectl get pods --namespace logging -l "app=nifi-frontend" -o jsonpath="{.items[0].metadata.name}")
$ kubectl port-forward $POD_NAME --namespace logging 8080:8080
```
Visit http://localhost:8080/nifi
![alt text](nifi-blank.png "Nifi Blank")
## Config Nifi
### Update Template
![alt text](nifi-upload-template.png "Nifi Upload Template")
![alt text](nifi-add-template.png "Nifi Add Template")
### Config ConsumeKafka Processer
![alt text](nifi-consumekafka-processer.png "ConsumeKafka Processer")
![alt text](nifi-consumekafka-settings.png "ConsumeKafka Settings")
### Config Two PutAzureBlob Processers
![alt text](nifi-putazureblob-processer-1.png "PutAzureBlob Processer 1")
![alt text](nifi-putazureblob-processer-2.png "PutAzureBlob Processer 2")

![alt text](nifi-blob-account-name.png "Account Name")
![alt text](nifi-blob-access-key.png "Access Key")

Start the flow
